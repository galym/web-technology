@extends('layouts.app')

@section("title", "Создать заголовок")

@section("style")
    <style>
        .heading-form {
            margin: auto;
            width: 500px;
        }
    </style>
@stop

@section('content')

    {!! Form::open(['url' => '/admin/heading', 'class' => 'heading-form', 'method' => 'POST']) !!}

        @include('admin.heading._form')

    {!! Form::close() !!}

@stop
