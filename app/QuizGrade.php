<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizGrade extends Model
{
    public function quiz_opening()
    {
        return $this->belongsTo('App\QuizOpening');
    }

    public function student()
    {
        return $this->belongsTo('App\User');
    }
}
