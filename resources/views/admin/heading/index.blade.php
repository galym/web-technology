@extends('layouts.app')
@section("title", "Заголовки")
@section('content')
    <a href="/admin/heading/create" class="btn-primary btn">
        Добавить заголовок
    </a>
    <table class="table">
        <tr>
            <th>Id</th>
            <th>Заголовок</th>
            <th>Подзаголовок</th>
            <th></th>
        </tr>
        @foreach($headings as $heading)
            <tr>
                <td>{{ $heading->id }}</td>
                <td>{{ $heading->parent ? $heading->parent->name : '' }}</td>
                <td>{{ $heading->name }}</td>
                <td>
                    <a href="/admin/heading/{{ $heading->id }}/edit" >
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a href="/admin/heading/{{ $heading->id }}/remove" >
                        <i class="fa fa-times"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </table>

@endsection
