<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/user-assets/images/favicon.ico" type="image/x-icon">


    <title>@yield('title') | {{ config('app.name') }}</title>

    <!-- Bootstrap core CSS -->

    <link href="/user-assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="/user-assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="/user-assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="/user-assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/maps/jquery-jvectormap-2.0.3.css" />
    <link href="/user-assets/css/icheck/flat/green.css" rel="stylesheet" />
    <link href="/user-assets/css/floatexamples.css" rel="stylesheet" type="text/css" />

    @yield('styles')

    <script src="/user-assets/js/jquery.min.js"></script>

    <style>
        .paper  {
            margin: 0 40px;
            padding: 100px;
            box-shadow: 1px 1px 20px -2px rgba(0, 0, 0, 0.5);
        }
    </style>
</head>
<body class="nav-md">

    <div class="container body">
        <div class="main_container">

            @if(Auth::guard("user")->check())
                @include('inc.user-sidebar')
            @else
                @include('inc.sidebar')
            @endif

            @include('inc.header')

            <!-- page content -->
            <div class="right_col" role="main">
                @if(Session::has("message"))
                    <br>
                    <div class="alert alert-{{ Session::get("message")['status'] }}">
                        {{ Session::get("message")['text'] }}
                    </div>
                @endif
                @if(Auth::guard("user")->check())
                    <div class="paper">
                        @yield('content')
                    </div>
                @else
                    @yield('content')
                @endif

                @include('inc.footer')
            </div>
            <!-- /page content -->

        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="/user-assets/js/bootstrap.min.js"></script>

    @yield("scripts")

    <!-- bootstrap progress js -->
    <script src="/user-assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="/user-assets/js/nicescroll/jquery.nicescroll.min.js"></script>

    <script src="/user-assets/js/custom.js"></script>


    <!-- /footer content -->
</body>

</html>
