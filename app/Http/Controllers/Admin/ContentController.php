<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use App\Heading;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    public function index()
    {
        $contents = Content::all();
        return view("admin.content.index", compact("contents"));
    }

    public function create()
    {
        $headingParents = DB::table('headings as h1')
            ->leftJoin('headings as h2', 'h2.parent_id', '=', 'h1.id')
            ->whereNull('h2.id')
            ->select('h1.*')->groupBy('h1.id')->pluck('name', 'id');
        return view("admin.content.create", compact("headingParents"));
    }

    public function store(Request $request)
    {
        $content = new Content();
        $content->heading_id = $request->heading_id;
        $content->text = $request->text;
        $content->save();
        return redirect("/admin/content");
    }

    public function edit($id)
    {
        $headingParents = DB::table('headings as h1')
            ->leftJoin('headings as h2', 'h2.parent_id', '=', 'h1.id')
            ->whereNull('h2.id')
            ->select('h1.*')->groupBy('h1.id')->pluck('name', 'id');
        $content = Content::find($id);
        return view("admin.content.edit", compact("headingParents", "content"));
    }

    public function update(Request $request, $id)
    {
        $content = Content::find($id);
        $content->heading_id = $request->heading_id;
        $content->text = $request->text;
        $content->save();
        return redirect("/admin/content");
    }
}
