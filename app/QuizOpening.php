<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizOpening extends Model
{
    protected $fillable = [
        'quiz_id', 'open_date', 'close_date'
    ];

    public function grade()
    {
        return $this->hasOne('App\QuizGrade');
    }

    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }
}
