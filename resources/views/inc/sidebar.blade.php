<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="/admin/home" class="site_title"><i class="fa fa-book"></i> <span>ADMIN</span></a>
        </div>
        <div class="clearfix"></div>

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Welcome</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="/admin/heading">
                            <i class="fa fa-home"></i> Заголовки
                        </a>
                    </li>
                    <li>
                        <a href="/admin/content">
                            <i class="fa fa-sticky-note"></i> Содержание
                        </a>
                    </li>
                    <li>
                        <a href="/admin/quiz">
                            <i class="fa fa-sticky-note"></i> Quiz
                        </a>
                    </li>
                    <li>
                        <a href="/admin/groups">
                            <i class="fa fa-users"></i> Группы
                        </a>
                    </li>
                    <li>
                        <a href="/admin/user">
                            <i class="fa fa-user"></i> Студенты
                        </a>
                    </li>
                    <li>
                        <a href="#" class="logout" onclick="logout()">
                            <i class="fa fa-sign-out"></i> Выйти
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>
<div class="logoutForm" style="display: none;">
    {!! Form::open(['url' => '/admin/logout', 'method' => 'POST']) !!}
    {!! Form::close(); !!}
</div>
<script>
    function logout() {
        $(".logoutForm form").submit();
    }
</script>