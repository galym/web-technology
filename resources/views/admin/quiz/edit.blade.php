@extends('layouts.app')

@section("title", "Изменить quiz")

@section("style")
    <style>
        .quiz-form {
            margin: auto;
            width: 500px;
        }
    </style>
@stop

@section('content')

    {!! Form::model($quiz, ['url' => '/admin/quiz/'. $quiz->id, 'class' => 'quiz-form', 'method' => 'PUT']) !!}

        @include('admin.quiz._form')

    {!! Form::close() !!}

@stop
