<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heading extends Model
{
    public function parent() {
        return $this->belongsTo('App\Heading', 'parent_id', 'id');
    }

    public function children() {
        return $this->hasMany('App\Heading', 'parent_id', 'id');
    }

    public function quizzes() {
        return $this->hasMany('App\Quiz');
    }
}
