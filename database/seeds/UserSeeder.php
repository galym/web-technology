<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new \App\Admin();
        $admin->name = 'Admin';
        $admin->email = 'admin@dev.kz';
        $admin->password = \Illuminate\Support\Facades\Hash::make('123qwe123');
        $admin->save();
    }
}
