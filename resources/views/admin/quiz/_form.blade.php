<div class="form-group">
    {!! Form::label('heading_id', 'Тест к заголовку', ['class' => 'control-label']) !!}
    {!! Form::select('heading_id', $headingParents, null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::label('name', 'Название: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<button class="btn btn-primary">
    {{ isset($quiz) ? 'Сохранить' : 'Создать' }}
</button>