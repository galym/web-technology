@extends('layouts.app')

@section("title", "Создать quiz")

@section("style")
    <style>
        .quiz-form {
            margin: auto;
            width: 500px;
        }
    </style>
@stop

@section('content')

    {!! Form::open(['url' => '/admin/quiz', 'class' => 'quiz-form', 'method' => 'POST']) !!}

        @include('admin.quiz._form')

    {!! Form::close() !!}

@stop
