<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\QuizGrade;
use App\Traits\RedirectWithMessage;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use RegistersUsers;
    use RedirectWithMessage;

    public function grades($id)
    {
        $user = User::find($id);
        return view("admin.user.grades", compact("user"));
    }

    public function index(Request $request)
    {
        $groups = Group::all()->pluck("name", 'id')->toArray();
        array_unshift($groups, 'Все группы');

        if ($request->group_id) {
            $users = User::where("group_id", $request->group_id)->paginate(20);
        } else {
            $users = User::all();
        }
        return view("admin.user.index", compact("users", "groups"));
    }

    public function create()
    {
        $groups = Group::all()->pluck('name', 'id');
        return view("admin.user.create", compact("groups"));
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->group_id = $request->group_id;
        $user->email = $request->email;
        if ($request->password_confirmed != $request->password) {
            return $this->error("Неверный пароль ");
        }
        $user->password = Hash::make($request->password);
        $user->save();

        return $this->success("Вы изменили данные студента", '/admin/user');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $groups = Group::all()->pluck('name', 'id');
        return view("admin.user.edit", compact( "user", 'groups'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->group_id = $request->group_id;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return $this->success("Вы изменили данные студента");
    }

    public function resetPassword($id) {
        $user = User::find($id);
        $user->password = Hash::make("123456");
        $user->save();

        return $this->success( "Пароль успешно сброшен на '123456'");
    }

    public function gradeRemove($id)
    {
        $grade = QuizGrade::find($id);
        $grade->delete();
        return $this->success('Оценка отменена');
    }

}
