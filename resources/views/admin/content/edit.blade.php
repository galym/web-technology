@extends('layouts.app')

@section("title", "Изменить содержание")

@section("styles")
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="/user-assets/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="/user-assets/css/editor/index.css" rel="stylesheet">
@stop

@section('content')

    {!! Form::model($content, ['url' => '/admin/content/'. $content->id, 'class' => 'content-form', 'method' => 'PUT']) !!}

        @include('admin.content._form')

    {!! Form::close() !!}

@stop
@section("scripts")
    <script src="/user-assets/js/editor/bootstrap-wysiwyg.js"></script>
    <script src="/user-assets/js/editor/external/jquery.hotkeys.js"></script>
    <script src="/user-assets/js/editor/external/google-code-prettify/prettify.js"></script>

    <!-- editor -->
    <script>
        $(document).ready(function() {
            $('.xcxc').click(function() {
                $('#descr').val($('#editor').html());
            });
        });

        $(function() {
            function initToolbarBootstrapBindings() {
                var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                        'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                        'Times New Roman', 'Verdana'
                    ],
                    fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                $.each(fonts, function(idx, fontName) {
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                });
                $('a[title]').tooltip({
                    container: 'body'
                });
                $('.dropdown-menu input').click(function() {
                    return false;
                })
                    .change(function() {
                        $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                    })
                    .keydown('esc', function() {
                        this.value = '';
                        $(this).change();
                    });

                $('[data-role=magic-overlay]').each(function() {
                    var overlay = $(this),
                        target = $(overlay.data('target'));
                    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                });
                if ("onwebkitspeechchange" in document.createElement("input")) {
                    var editorOffset = $('#editor').offset();
                    $('#voiceBtn').css('position', 'absolute').offset({
                        top: editorOffset.top,
                        left: editorOffset.left + $('#editor').innerWidth() - 35
                    });
                } else {
                    $('#voiceBtn').hide();
                }
            };

            function showErrorAlert(reason, detail) {
                var msg = '';
                if (reason === 'unsupported-file-type') {
                    msg = "Unsupported format " + detail;
                } else {
                    console.log("error uploading file", reason, detail);
                }
                $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
            };
            initToolbarBootstrapBindings();
            $('#editor').wysiwyg({
                fileUploadError: showErrorAlert
            });
            window.prettyPrint && prettyPrint();
        });
    </script>
    <!-- /editor -->

    <script>
        function submitForm() {
            var content = $('#editor').html();
            $('#content').val(content);
            $(".content-form").submit();
        }
    </script>

@stop