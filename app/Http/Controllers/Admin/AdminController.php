<?php

namespace App\Http\Controllers\Admin;

use App\Heading;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function home()
    {
        $headings = Heading::all();
        return view("admin.heading.index", compact("headings"));
    }
}
