<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Question;
use App\Quiz;
use App\Traits\RedirectWithMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    use RedirectWithMessage;

    public function quiz($quiz_id)
    {
        $quiz = Quiz::find($quiz_id);
        return view("admin.quiz.questions.index", compact("quiz"));
    }

    public function import(Request $request)
    {
        $txtContents = \File::get($request->file('question_file'));
        $arr = preg_split('#\n#', $txtContents);
        $quiz_id = $request->quiz_id;

        $question = new Question();
        foreach ($arr as $content) {
            if ( substr($content, 0, 2) == 'Q.') {
                $question = new Question();
                $question->question = substr($content, 2, strlen($content));
                $question->quiz_id = $quiz_id;
                $question->save();
            } else {
                $answer = new Answer();
                $answer->question_id = $question->id;
                $answer->is_correct = substr($content, 0, 2) == 'C.';
                $answer->answer = substr($content, 2, strlen($content));
                $answer->save();
            }
        }
        return $this->success('Вы успешно импортировали вопросы');
    }

    public function questions($quiz_id)
    {
        $quiz = Quiz::find($quiz_id);
        $questions = [];
        foreach($quiz->questions as $question) {
            array_push($questions, [
                'id' => $question->id,
                'quiz_id' => $question->quiz_id,
                'question' => $question->question,
                'answers' => $question->answers ? $question->answers : [],
            ]);
        }
        return $questions;
    }

    public function removeAnswer($answer_id)
    {
        $answer = Answer::find($answer_id);
        $answer->delete();
    }

    public function saveQuestion(Request $request)
    {
        $question = new Question();
        if (isset($request->question['id'])) {
            $question = Question::find($request->id);
        }
        $question->quiz_id = $request->question['quiz_id'];
        $question->question = $request->question['question'];
        $question->save();
        foreach ($request->question['answers'] as $answer) {
            $answerObj = new Answer();
            if (isset($answer["id"])) {
                $answerObj = Answer::find($request->id);
            }
            $answerObj->answer = $answer["answer"];
            $answerObj->question_id = $question->id;
            $answerObj->is_correct = $answer["is_correct"] == 'false' ? 0 : 1;
            $answerObj->save();
        }

        dd($request->question['answers']);
    }

    public function removeQuestion($question_id)
    {
        $question = Question::find($question_id);
        foreach ($question->answers as $answer) {
            $answer = Answer::find($answer->id);
            $answer->delete();
        }

        $question->delete();
    }
}
