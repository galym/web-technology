@extends('layouts.app')
@section("title", "Содержание")
@section('content')
    <a href="/admin/content/create" class="btn-primary btn">
        Добавить содержание
    </a>
    <table class="table">
        <tr>
            <th>Id</th>
            <th>Заголовок</th>
            <th>Текст</th>
            <th></th>
        </tr>
        @foreach($contents as $content)
            <tr>
                <td>{{ $content->id }}</td>
                <td>{{ $content->heading ? $content->heading->name : '' }}</td>
                <td>
                    <a href="/admin/content/{{ $content->id }}/edit" >
                        <i class="fa fa-pencil"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </table>

@endsection
