<div class="form-group">
    {!! Form::label('parent_id', 'Родительский заголовок: ', ['class' => 'control-label']) !!}
    {!! Form::select('parent_id', $headingParents, null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::label('name', 'Название загловка: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<button class="btn btn-primary">
    {{ isset($heading) ? 'Сохранить' : 'Создать' }}
</button>