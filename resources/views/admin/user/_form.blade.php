
<div class="form-group">
    {!! Form::label('group_id', 'Группа', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('group_id', $groups, null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('name', 'Имя', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Введите имя студента']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('surname', 'Фамилия', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('surname', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Введите фамилию студента']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::email('email', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Введите e-mail']) !!}
    </div>
</div>

@if (! isset($user))
    <div class="form-group">
        {!! Form::label('password', 'Пароль', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::password('password', ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Введите пароль']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('password_confirmed', 'Повторите пароль', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}

        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::password('password_confirmed', ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Повторите пароль ']) !!}
        </div>
    </div>
@endif
<div class="col-md-9 text-right">
    @if (isset($user))
        <a href="/admin/user/{{ $user->id }}/reset-password" class="btn btn-danger"
                onclick="if(!confirm('Вы действительно хотите сбросить пароль для этого студента?')) { return false; }">
            Сбросить пароль
        </a>
    @endif
    <button class="btn btn-primary">
        {{ isset($user) ? 'Сохранить' : 'Создать' }}
    </button>
</div>
