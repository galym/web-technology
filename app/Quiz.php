<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $table = "quizzes";

    public function heading()
    {
        return $this->belongsTo('App\Heading');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function openings() {
        return $this->hasMany('App\QuizOpening');
    }

    public function active_opening() {
        $current = new \DateTime();
        $current->setTimeZone(new \DateTimeZone('Asia/Almaty'));
        return $this->hasMany('App\QuizOpening')
            ->where('open_date', '<=', $current->format('Y-m-d H:i'))
            ->where('close_date', '>', $current->format('Y-m-d H:i'))->first();
    }

    public function grade() {
        return $this->hasOne('App\QuizGrade');
    }
}
