@extends('layouts.app')
@section("title", "Студенты")
@section("styles")
    <style>
        .groupSelect {
            width: 200px;
        }
    </style>
@endsection
@section("scripts")
    <script type="text/javascript" src="/js/url-library.js"></script>
    <script>
        $(".groupSelect").on("change", function () {
            var group = $(this).val();
            if (group == 0) {
                queryString.removeAndRedirect("group_id");
            } else {
                queryString.pushAndRedirect("group_id", group);
            }
        });
    </script>
@endsection
@section('content')

    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Список студентов</h2>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <a href="/admin/user/create" class="btn-primary btn">
                    Добавить студента
                </a>
                {!! Form::select('group', $groups, Request::get('group_id'), ['class' => 'form-control groupSelect pull-right']) !!}

                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Группа</th>
                        <th>Почта</th>
                        <th>Оценка</th>
                        <th>Изменить</th>
                        <th></th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->surname }}</td>
                            <td>{{ $user->group ? $user->group->name : '' }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a href="/admin/user/{{ $user->id }}/grades">
                                    {{ $user->grade() }} %
                                </a>
                            </td>
                            <td>
                                <a href="/admin/user/{{ $user->id }}/edit" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>


@endsection
