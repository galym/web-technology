<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Traits\RedirectWithMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    use RedirectWithMessage;


    public function index()
    {
        $groups = Group::all();
        return view("admin.group.index", compact("groups"));
    }


    public function store(Request $request)
    {
        Group::create($request->all());
        return $this->success('Вы успешно добавили группу');
    }

    public function update(Request $request, $id)
    {
        $group = Group::find($id);
        $group->update($request->all());
        return $this->success('Вы успешно отредактировали группу');
    }

    public function destroy( $id)
    {
        $group = Group::find($id);
        $group->forceDelete();
        return $this->success('Вы успешно удалили группу');
    }

}
