<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Quiz;
use App\QuizOpening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuizController extends Controller
{
    public function index()
    {
        $quizzes = Quiz::all();
        return view("admin.quiz.index", compact("quizzes"));
    }

    public function create()
    {
        $headingParents = DB::table('headings as h1')
            ->leftJoin('headings as h2', 'h2.parent_id', '=', 'h1.id')
            ->whereNull('h2.id')
            ->select('h1.*')->groupBy('h1.id')->pluck('name', 'id');

        return view("admin.quiz.create", compact("headingParents"));
    }

    public function store(Request $request)
    {
        $quiz = new Quiz();
        $quiz->heading_id = $request->heading_id;
        $quiz->name = $request->name;
        $quiz->save();
        return redirect("/admin/quiz");
    }

    public function edit($id)
    {
        $headingParents = DB::table('headings as h1')
            ->leftJoin('headings as h2', 'h2.parent_id', '=', 'h1.id')
            ->whereNull('h2.id')
            ->select('h1.*')->groupBy('h1.id')->pluck('name', 'id');
        $quiz = Quiz::find($id);
        return view("admin.quiz.edit", compact("headingParents", "quiz"));
    }

    public function update(Request $request, $id)
    {
        $quiz = Quiz::find($id);
        $quiz->heading_id = $request->heading_id;
        $quiz->name = $request->name;
        $quiz->save();
        return redirect("/admin/quiz");
    }

    public function open(Request $request)
    {
        QuizOpening::create($request->all());
        return redirect("/admin/quiz");
    }
}
