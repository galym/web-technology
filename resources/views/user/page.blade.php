@extends('layouts.app')
@section("title", $heading->name)
@section("styles")
    <style>
        .quiz {
            font-size: 18px;
        }
        .question .title {
            font-weight: bolder;
        }
        .none {
            display: none;
        }
        .question .answer {
            display: flex;
        }
    </style>
@endsection
@section('content')
    {!! $content->text !!}


    @if(isset($quiz) && $quiz->active_opening())
        <hr>
        @if ($quiz->active_opening()->grade)
            <h2>
                Ваш результат при тестировании: {{ $quiz->active_opening()->grade->grade }}
            </h2>
        @else
            <a href="/user/quiz/{{ $quiz->id }}" class="btn btn-primary">
                Пройти тестирование
            </a>
        @endif
    @endif
@endsection
