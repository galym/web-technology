<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.02.2019
 * Time: 13:20
 */

namespace App\Traits;


trait RedirectWithMessage
{
    public function unexpectedError($url = null)
    {
        return $this->error('Произошла непредвиденная ошибка', $url);
    }

    public function success($message, $url = null)
    {
        return $this->redirect($message, 'success', $url);
    }

    public function error($message, $url = null)
    {
        return $this->redirect($message, 'error', $url);
    }

    public function warning($message, $url = null)
    {
        return $this->redirect($message, 'warning', $url);
    }

    public function redirect($message, $status, $url = null)
    {
        $message = [
          'status' => $status,
          'text' => $message
        ];

        if ($url) {
            return redirect($url)->with('message', $message);
        }

        return back()->with('message', $message);
    }
}