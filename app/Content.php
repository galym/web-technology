<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public function heading()
    {
        return $this->hasOne('App\Heading', 'id', 'heading_id');
    }
}
