<?php

/*Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('user')->user();

    //dd($users);

    return view('user.home');
})->name('home');*/

Route::get('/home', 'Cabinet\CabinetController@home');

Route::get('/quiz/{id}', 'Cabinet\CabinetController@quiz');
Route::post('/quiz/{id}', 'Cabinet\CabinetController@saveQuiz');

Route::get('/search', 'Cabinet\CabinetController@search');

Route::resource('/material', 'Cabinet\CabinetController');

