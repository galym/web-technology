@extends('layouts.app')
@section("title", "Группы")
@section("styles")
    <style>
        .deleteForm {
            display: none;
        }
        .table {
            font-size: 16px;
        }
    </style>
@endsection
@section('content')


    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Список групп</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <a href="#" class="btn-primary btn" data-toggle="modal" data-target="#add-group-modal">
                    Добавить группу
                </a>
                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>Название</th>
                        <th>Действия</th>
                    </tr>
                    @foreach($groups as $group)
                        <tr>
                            <td>{{ $group->id }}</td>
                            <td>{{ $group->name }}</td>
                            <td>
                                <a href="#" class="btn btn-primary" onclick="editGroup('{{ $group->id }}', '{{ $group->name }}')" data-toggle="modal" data-target="#edit-group-modal">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" class="btn btn-danger" onclick="removeGroup('{{ $group->id }}')" >
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>


    <div id="add-group-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Добавить группу</h4>
                </div>
                {!! Form::open(['url' => '/admin/groups', 'method' => 'post']) !!}
                    <div class="modal-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Название: ', ['class' => 'control-label']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary" value="Сохранить"/>
                        <a class="btn btn-default" data-dismiss="modal">Закрыть</a>
                    </div>
                {!! Form::close(); !!}
            </div>

        </div>
    </div>

    <div id="edit-group-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Редактировать группу</h4>
                </div>
                {!! Form::open(['method' => 'PUT']) !!}
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label('name', 'Название: ', ['class' => 'control-label']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Сохранить"/>
                    <a class="btn btn-default" data-dismiss="modal">Закрыть</a>
                </div>
                {!! Form::close(); !!}
            </div>

        </div>
    </div>

    <div class="deleteForm">
        {!! Form::open(['method' => 'DELETE']) !!}
        {!! Form::close(); !!}
    </div>
@endsection
@section("scripts")
    <script>
        function editGroup(id, name) {
            var url = "/admin/groups/" +id;
            $("#edit-group-modal form").attr("action", url);
            $("#edit-group-modal form .form-group input").val(name);
        }

        function removeGroup(id) {
            if(! confirm("Вы дейсвтительно хотите удалить эту группу")) {
                return;
            }
            var url = "/admin/groups/" +id;
            $(".deleteForm form").attr("action", url);
            $(".deleteForm form").submit();
        }
    </script>
@endsection
