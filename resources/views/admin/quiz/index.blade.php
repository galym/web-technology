@extends('layouts.app')
@section("title", "Quiz")
@section('content')
    <a href="/admin/quiz/create" class="btn-primary btn">
        Добавить новый quiz
    </a>
    <table class="table">
        <tr>
            <th>Id</th>
            <th>Заголовок</th>
            <th>Название</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        @foreach($quizzes as $quiz)
            <tr>
                <td>{{ $quiz->id }}</td>
                <td>{{ $quiz->heading->name }}</td>
                <td>{{ $quiz->name }}</td>
                <td>
                    <a href="/admin/quiz/{{ $quiz->id }}/edit" >
                        Изменить
                    </a>
                </td>
                <td>
                    <a href="/admin/quiz/{{ $quiz->id }}/questions" >
                        Вопросы
                    </a>
                </td>
                <td>
                    <a href="#" onclick="$('.quizId').val('{{ $quiz->id }}')" data-toggle="modal" data-target="#addQuizModal">
                        Открыть
                    </a>
                    <br>
                    @foreach($quiz->openings as $op)
                        {{ $op->open_date }}
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>

    <div id="addQuizModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Открыть quiz</h4>
                </div>
                {!! Form::open(['url' => '/admin/quiz/open', 'method' => 'post']) !!}
                    <div class="modal-body">
                        <input type="hidden" name="quiz_id" class="quizId">
                        <div class="form-group">
                            {!! Form::label('open_date', 'Дата открытия: ', ['class' => 'control-label']) !!}
                            {!! Form::text('open_date', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('close_date', 'Дата закрытия: ', ['class' => 'control-label']) !!}
                            {!! Form::text('close_date', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary" value="Сохранить"/>
                        <a class="btn btn-default" data-dismiss="modal">Закрыть</a>
                    </div>
                {!! Form::close(); !!}
            </div>

        </div>
    </div>

@endsection
