@extends('layouts.app')

@section("title", "Редактировать студента")

@section('content')

    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Редактировать студента </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {!! Form::model($user, ['url' => '/admin/user/'. $user->id, 'class' => 'form-horizontal form-label-left', 'method' => 'PUT']) !!}

                    @include('admin.user._form')

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop
