<?php

Route::get('/home', 'Admin\AdminController@home');

Route::get('/heading/{id}/remove', 'Admin\HeadingController@remove');
Route::resource('/heading', 'Admin\HeadingController');

Route::resource('/content', 'Admin\ContentController');

Route::get('/quiz/{id}/questions', "Admin\QuestionController@quiz");
Route::get('/quiz/{id}/questions/get', "Admin\QuestionController@questions");
Route::post('/quiz/{id}/questions/save', "Admin\QuestionController@saveQuestion");
Route::post('/quiz/{id}/answers/{answer_id}/remove', "Admin\QuestionController@removeAnswer");

Route::post('/quiz/questions/{question_id}/remove', "Admin\QuestionController@removeQuestion");
Route::post('/quiz/questions/import', "Admin\QuestionController@import");

Route::post('/quiz/open', 'Admin\QuizController@open');
Route::resource('/quiz', 'Admin\QuizController');
Route::resource('/quiz', 'Admin\QuizController');

Route::post('/user/register', 'Admin\UserController@register');
Route::get('/user/{id}/reset-password', 'Admin\UserController@resetPassword');
Route::get('/user/{id}/grades', 'Admin\UserController@grades');
Route::get('/user/grades/{grade_id}/remove', 'Admin\UserController@gradeRemove');

Route::resource('/user', 'Admin\UserController');

Route::resource('/groups', 'Admin\GroupController');


Route::resource('/', 'Admin\AdminController');