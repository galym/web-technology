<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="/user/home" class="site_title"><i class="fa fa-book"></i> <span>WEB-TECH</span></a>
        </div>
        <div class="clearfix"></div>

        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
                <h3>Материалы</h3>
                <ul class="nav side-menu">
                    @foreach(\App\Heading::whereNull('parent_id')->get() as $heading)
                        <li>
                            @if ($heading->children->count() > 0 )
                                <a >
                                    <i class="fa fa-book"></i> {{ $heading->name }} <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu" style="display: none">
                                    @foreach($heading->children as $child)
                                        <li>
                                            <a href="{!! url('/user/material/' . $child->id) !!}">{{ $child->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <a href="{!! url('/user/material/' . $heading->id) !!}">
                                    <i class="fa fa-book"></i> {{ $heading->name }}
                                </a>
                            @endif
                        </li>
                    @endforeach
                    <li>
                        <a href="#" class="logout" onclick="logout()">
                            <i class="fa fa-sign-out"></i> Выйти
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="logoutForm" style="display: none;">
    {!! Form::open(['url' => '/user/logout', 'method' => 'POST']) !!}
    {!! Form::close(); !!}
</div>
<script>
    function logout() {
        $(".logoutForm form").submit();
    }
</script>