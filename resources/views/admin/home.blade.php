@extends('layouts.app')

@section('content')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addHeadingModal"
            xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml"
            xmlns:v-on="http://www.w3.org/1999/xhtml">
        Добавить заголовок
    </button>

    <table class="table">
        <tr>
            <th>Id</th>
            <th>Заголовок</th>
            <th>Подзаголовок</th>
        </tr>
        @foreach($headings as $heading)
            <tr>
                <td>{{ $heading->id }}</td>
                <td>{{ $heading->parent->name }}</td>
                <td>{{ $heading->name }}</td>
            </tr>
        @endforeach
    </table>

    <!-- Modal -->
    <div id="addHeadingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ action }} заголовка</h4>
                </div>
                <div class="modal-body">
                    <label for="name">Название</label>
                    <input type="text" v-model="headingName" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" v-on:click="save">Сохранить</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section("scripts")
    <script>
        var addHeadingModal = new Vue({
            el: '#addHeadingModal',
            data: {
                headingName: '',
                action: 'Добавление'
            },
            methods: {
                save: function () {
                    alert(this.headingName);
                },
            }
        })
    </script>
@endsection
