@extends('layouts.app')

@section("styles")
    <style>
        .quiz {
            font-size: 18px;
        }
        .question .title {
            font-weight: bolder;
            font-size: 18px;
        }

        .question .answer {
            font-size: 17px;
            margin-left:20px;
        }
        .answer .text {
            margin-left:10px;
        }
        .none {
            display: none;
        }
        .question .answer {
            display: flex;
        }
    </style>
@endsection
@section('content')
    <span class="none">
        {{{ '' != $i = 1 }}}
    </span>

    @if(isset($grade))
        <h1>
            Результат: {{ $grade->grade }} %
        </h1>
    @elseif(isset($opening))
        {!! Form::open(['method' => 'POST']) !!}
            @foreach($quiz->questions as $question)
                <div class="question">
                    <div class="title">
                        {{{ $i++ }}}. {{ $question->question }}
                    </div>
                    @foreach($question->answers as $answer)
                        <div class="answer">
                            <input type="radio" name="answer[{{ $question->id }}]" value="{{ $answer->id }}">
                            <span class="text">
                            {{ $answer->answer }}
                        </span>
                        </div>
                    @endforeach
                </div>
            @endforeach
            <button class="btn btn-primary">Отправить</button>
        {!! Form::close() !!}
    @else
        <h1>Данный quiz еще не открыт</h1>
    @endif

@endsection
