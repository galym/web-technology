<?php

namespace App\Http\Controllers\Cabinet;

use App\Answer;
use App\Content;
use App\Heading;
use App\Question;
use App\Quiz;
use App\QuizGrade;
use App\Traits\RedirectWithMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CabinetController extends Controller
{
    use RedirectWithMessage;

    public function home()
    {
        return view('user.home');
    }

    public function quiz($id)
    {
        $quiz = Quiz::find($id);
        $opening = $quiz->active_opening();
        if ($opening) {
            $grade = $opening->grade;
        }
        return view('user.quiz', compact('quiz', 'grade', 'opening'));
    }

    public function show($id)
    {
        $content = Content::where('heading_id', $id)->first();
        $heading = Heading::find($id);
        $quizzes = $heading->quizzes;
        if ($quizzes->count() > 0) {
            $userId = Auth::user()->id;
            $quiz = $quizzes[$userId % $quizzes->count()];
        }

        return view('user.page', compact('content', 'heading', 'quiz'));
    }

    public function saveQuiz(Request $request, $id)
    {
        $quiz_id = null;
        $correctAnswersCount = 0;
        foreach ($request->answer as $question_id => $answer_id) {
            if (! $quiz_id) {
                $quiz_id = Question::find($question_id)->quiz_id;
            }

            $answer = Answer::find($answer_id);
            if ($answer->isCorrect) {
                $correctAnswersCount++;
            }
        }
        if (! $quiz_id) {
            return $this->unexpectedError();
        }
        $quiz = Quiz::find($quiz_id);
        $opening = $quiz->active_opening();
        if (! $quiz->active_opening()) {
            return $this->unexpectedError();
        }

        $questionCounts = $quiz->questions->count();
        $quizGrade = new QuizGrade();
        $quizGrade->quiz_opening_id = $opening->id;
        $quizGrade->user_id = Auth::user()->id;
        $quizGrade->grade = $correctAnswersCount == 0 ? 0 : $questionCounts / $correctAnswersCount;
        $quizGrade->save();
        return $this->success('Ваши результаты');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $headings = DB::table('headings')
            ->leftJoin('contents', 'headings.id', '=', 'contents.heading_id')
            ->where(function ($query) use ($search) {
                $query->where("headings.name", 'like', '%'.$search.'%')
                    ->orWhere("contents.text", 'like', '%'.$search.'%');
            })->whereNotNull("parent_id")
            ->select('headings.*')
            ->get();
        return view("user.search", compact('headings', 'search'));

    }

}
