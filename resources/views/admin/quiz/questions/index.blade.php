@extends('layouts.app')
@section("title", "Вопросы")
@section("styles")
    <style>
        .question-title {
            display: flex;
        }
    </style>
@endsection
@section('content')
    <h1>Вопросы к тесту {{ $quiz->name }}</h1>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#import-questions-modal">
        Импортировать вопросы
    </button>
    <div id="quiz">
        <div id="questions" v-for="(question, index ) in questions" class="panel panel-info">
            <div class="panel-heading question-title">
                <div>
                     Вопрос #@{{ index + 1 }}
                </div>
                <input type="text" v-model="question.question" class="form-control" placeholder="Введите вопрос">
                <button class="btn btn-danger" v-on:click="removeQuestion(index)">
                    <i class="fa fa-times"></i>
                </button>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr v-for="(answer, answer_index) in question.answers">
                        <td>
                            Ответ #@{{ answer_index + 1 }}
                        </td>
                        <td>
                            <input type="text" v-model="answer.answer" class="form-control" placeholder="Введите ответ">
                        </td>
                        <td>
                            <input type="checkbox" v-model="answer.is_correct">
                        </td>
                        <td>
                            <a class="btn btn-danger" v-on:click="removeAnswer(question, answer_index)">
                                <i class="fa fa-remove"></i>
                            </a>
                        </td>
                    </tr>
                </table>
                <div class="text-center">
                    <a style="cursor: pointer;" v-on:click="addAnswer(question)">
                        <i class="fa fa-plus"></i> Добавить
                    </a>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button class="btn btn-success" v-on:click="save(question)">Сохранить</button>
            </div>
        </div>
        <button class="btn btn-primary" v-on:click="addQuestion">Добавить новый вопрос</button>
    </div>

    <div id="import-questions-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Импортировать вопросы</h4>
                </div>
                {!! Form::open(['url' => '/admin/quiz/questions/import', 'method' => 'POST', 'files' => true]) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="quiz_id" value="{{ $quiz->id }}">
                        {!! Form::label('question_file', 'Название: ', ['class' => 'control-label']) !!}
                        {!! Form::file('question_file', ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Сохранить"/>
                    <a class="btn btn-default" data-dismiss="modal">Закрыть</a>
                </div>
                {!! Form::close(); !!}
            </div>

        </div>
    </div>
@endsection
@section("scripts")
    <script type="text/javascript" src="/js/app.js"></script>
    <script>
        var quiz = new Vue({
            el: '#quiz',
            data: {
                questions: []
            },
            methods: {
                addQuestion: function () {
                    this.questions.push({
                        'question' : '',
                        'quiz_id' : '{{ $quiz->id }}',
                        'answers': [
                            {
                                'answer' : '',
                                'is_correct' : false
                            }
                        ]
                    })
                }, 
                addAnswer: function (question) {
                    question['answers'].push({
                        'answer' : '',
                        'is_correct' : false
                    })
                },
                removeQuestion: function(index) {
                    var e = this;
                    var question_id = e.questions[index]['id'];
                    if (question_id) {
                        $.post("/admin/quiz/questions/"+question_id+"/remove", {"_token": "{{ csrf_token() }}"}, function(data, status){
                            e.questions.splice(index, 1);
                        });
                    } else {
                        e.questions.splice(index, 1);
                    }
                },
                removeAnswer: function (question, index) {
                    var answer_id = question['answers'][index]['id'];
                    if (answer_id) {
                        $.post("/admin/quiz/{{ $quiz->id }}/answers/"+answer_id+"/remove", {"_token": "{{ csrf_token() }}"}, function(data, status){
                            question['answers'].splice(index, 1);
                            alert("Вопрос успешно удален");
                        });
                    } else {
                        question['answers'].splice(index, 1);
                        alert("Вопрос успешно удален");
                    }
                },
                save: function (question) {
                    var e = this;
                    $.post("/admin/quiz/{{ $quiz->id }}/questions/save", {question: question, "_token": "{{ csrf_token() }}"}, function(data, status){
                        alert("Вопрос успешно сохранен");
                    });
                }
            },
            created: function () {
                var e = this;
                $.get("/admin/quiz/{{ $quiz->id }}/questions/get", function(data, status){
                    e.questions = data;
                });
            }
        })
    </script>
@endsection
