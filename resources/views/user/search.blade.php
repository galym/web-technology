@extends('layouts.app')
@section("title", 'Результаты поиска "'.$search.'"')
@section("styles")
    <style>
        .link {
            font-size: 18px;
        }
    </style>
@endsection
@section('content')
    <h2>Результаты поиска '{{ $search }}' ({{ $headings->count() }})</h2>
    @forelse($headings as $heading)
        <div class="link">
            <a href="/user/material/{{ $heading->id }}">{{ $heading->name }}</a>
        </div>
    @empty
        <h2>Не найдено</h2>
    @endforelse
@endsection
