@extends('layouts.app')
@section("title", "Студенты")
@section("styles")
    <style>
        .groupSelect {
            width: 200px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="x_panel">
            <div class="x_title">
                <h2>Оценки студента: {{ $user->name ." ". $user->surname }}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table">
                    <tr>
                        <th>Id</th>
                        <th>Quiz</th>
                        <th>Оценка</th>
                        <th>Дата открытия</th>
                        <th>Дата закрытия</th>
                        <th>Дата создания</th>
                        <th>Удалить</th>
                    </tr>
                    @forelse ($user->grades as $grade)
                        <tr>
                            <td>{{ $grade->id }}</td>
                            <td>{{ $grade->quiz_opening->quiz->name }}</td>
                            <td>{{ $grade->grade }}</td>
                            <td>{{ $grade->quiz_opening->open_date }}</td>
                            <td>{{ $grade->quiz_opening->close_date }}</td>
                            <td>{{ $grade->created_at }}</td>
                            <td>
                                <a href="/admin/user/grades/{{ $grade->id }}/remove" class="btn btn-danger">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center" style="font-size: 18px;">
                                У студента нет оценок
                            </td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </div>


@endsection
