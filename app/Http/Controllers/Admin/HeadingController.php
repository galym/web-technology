<?php

namespace App\Http\Controllers\Admin;

use App\Heading;
use App\Http\Controllers\Controller;
use App\Traits\RedirectWithMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HeadingController extends Controller
{
    use RedirectWithMessage;

    public function index()
    {
        $headings = Heading::all();
        return view("admin.heading.index", compact("headings"));
    }

    public function create()
    {
        $headingParents = [];
        $hpList = Heading::whereNull('parent_id')->get();
        $headingParents[0] = "Нет родительского заголовка";
        foreach ($hpList as $heading) {
            $headingParents[$heading->id] = $heading->name;
        }
        return view("admin.heading.create", compact("headingParents"));
    }

    public function store(Request $request)
    {
        $heading = new Heading();
        $heading->parent_id = $request->parent_id == 0 ? null : $request->parent_id;
        $heading->name = $request->name;
        $heading->save();
        return redirect("/admin/heading");
    }

    public function edit($id)
    {
        $headingParents = [];
        $hpList = Heading::whereNull('parent_id')->get();
        $headingParents[0] = "Нет родительского заголовка";
        foreach ($hpList as $heading) {
            $headingParents[$heading->id] = $heading->name;
        }

        $heading = Heading::find($id);
        return view("admin.heading.edit", compact("headingParents", "heading"));
    }

    public function update(Request $request, $id)
    {
        $heading = Heading::find($id);
        $heading->parent_id = $request->parent_id == 0 ? null : $request->parent_id;
        $heading->name = $request->name;
        $heading->save();
        return redirect("/admin/heading");
    }

    public function remove($id)
    {
        $heading = Heading::find($id);
        $heading->delete();
        return $this->success("Вы успешно удалили заголовок");
    }
}
