@extends('layouts.app')

@section("title", "Изменить заголовок")

@section("style")
    <style>
        .heading-form {
            margin: auto;
            width: 500px;
        }
    </style>
@stop

@section('content')

    {!! Form::model($heading, ['url' => '/admin/heading/'. $heading->id, 'class' => 'heading-form', 'method' => 'PUT']) !!}

        @include('admin.heading._form')

    {!! Form::close() !!}

@stop
